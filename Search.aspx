﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Search.aspx.vb" Inherits="Search"
    MasterPageFile="~/MasterForHome.master" %>

<%--<%@ Register Src="~/UserControl/DashBoard.ascx" TagPrefix="uc1" TagName="DashBoard" %>--%>
<%@ Register Src="~/UserControl/FltSearch.ascx" TagName="IBESearch" TagPrefix="Search" %>
<%@ Register Src="~/UserControl/HotelSearch.ascx" TagPrefix="Search" TagName="HotelSearch" %>
<%@ Register Src="~/BS/UserControl/BusSearch.ascx" TagName="BusSearch" TagPrefix="Searchsss" %>
<%@ Register Src="~/UserControl/FltSearchFixDep.ascx" TagName="IBESearchDep" TagPrefix="SearchDep" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Cont1" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/ReissueRefund.js")%>" type="text/javascript"></script>
    <%--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>--%>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>

 <style>
     .ui-datepicker-month, .ui-datepicker-year {
         font-size: 26px;
    font-weight: 700;
     }


 </style>  

    <style>
.tooltip {
  position: relative;
  display: inline-block;
  border-bottom: 1px dotted black;
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 120px;
  background-color: black;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 5px 0;

  /* Position the tooltip */
  position: absolute;
  z-index: 1;
}

.tooltip:hover .tooltiptext {
  visibility: visible;
}
</style>

    
<style type="text/css">
        .mobiApp {
    background: -moz-linear-gradient(left, #334755, #334755);
    background: -webkit-linear-gradient(left, #334755, #334755);
    background: -ms-linear-gradient(left, #334755, #334755);
    background: -o-linear-gradient(left, #334755, #334755);
    height: 313px;
    border-top: 2px solid #07A77B;
    color: #d4dee5;
    margin-top: 110px;
}

        .u_floatL {
    float: left;
}

        .u_floatL {
    float: left;
}
        .mobiApp .head {
    margin: 0;
    padding: 0;
    margin-top: 50px;
    font-size: 28px;
    font-weight: 300;
    background:none !important;
}
        .u_fontW700 {
    font-weight: 700 !important;
}
.u_clViaWhite {
    color: #fff !important;
}

.u_clear {
    clear: both;
}

.mobiApp .u_inlineblk {
    vertical-align: top;
}
.u_inlineblk {
    display: inline-block;
}

.mobiApp .appDisc {
    border-right: 1px solid #d4dee5;
    font-size: 20px;
    padding: 0px 20px 0 0;
    font-weight: 300;
    margin-top: 20px;
}

.mobiApp h5 {
    margin-top: 10px;
    font-size: 16px;
    font-weight: 300;
    margin-bottom: 10px;
}

.mobiApp .appDisc p {
    margin: 5px 0px;
}

.INR {
    font-family: 'icomoon';
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    color: inherit;
    font-weight: inherit;
}

.u_fontW600 {
    font-weight: 600 !important;
}
.u_clViaWhite {
    color: #fff !important;
}

.mobiApp .appVia {
    padding: 0px 0 0 20px;
    margin-top: 20px;
}

.mobiApp h5 {
    margin-top: 10px;
    font-size: 16px;
    font-weight: 300;
    margin-bottom: 10px;
}

.mobiApp .coupCode {
    padding: 10px;
    text-align: center;
    background: #12B58A;
}

.mobiApp .appVia h4 {
    font-weight: 700;
    font-size: 20px;
    margin: 0px;
}
.u_clViaWhite {
    color: #fff !important;
}

.mobiApp .appStore {
    padding-top: 40px;
}


    </style>



    <style type="text/css">
        .holder { 
  background-color:#ccc;
  width:300px;
  height:250px;
  overflow:hidden;
  padding:10px;
  font-family:Helvetica;
}
.holder .mask {
  position: relative;
  left: 0px;
  top: 10px;
  width:300px;
  height:240px;
  overflow: hidden;
}
.holder ul {
  list-style:none;
  margin:0;
  padding:0;
  position: relative;
}
.holder ul li {
  padding:10px 0px;
}
.holder ul li a {
  color:darkred;
  text-decoration:none;
}
    </style>


    <%--<script type="text/javascript">
        function passchange() {
            $("#passchange1").fadeIn();
        }
        function passchangeclose() {
            $("#passchange1").fadeOut();
        }
        function passchangevalidation() {
            if ($("#ctl00_ContentPlaceHolder1_oldpwd").val() == "") {
                alert('Please enter old password');
                $("#ctl00_ContentPlaceHolder1_oldpwd").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_newpwd").val() == "") {
                alert('Please enter new password');
                $("#ctl00_ContentPlaceHolder1_newpwd").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_newpwdconf").val() == "") {
                alert('Please enter confirm new password');
                $("#ctl00_ContentPlaceHolder1_newpwdconf").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_newpwd").val() != $("#ctl00_ContentPlaceHolder1_newpwdconf").val()) {
                alert('New password and confirmation password is not matching.  ');
                $("#ctl00_ContentPlaceHolder1_newpwdconf").focus();
                return false;
            }
        }
    </script>--%>
    <style type="text/css">
        nav {
	    width: 100%;
    /* margin: 100px auto; */
    overflow: hidden;
    position: relative;
    right: -75px;	
}
        @media only screen and (max-width: 600px) {
            nav {
                right: 19px;
            }

        }

nav ul {
	list-style: none;
	overflow: hidden;
}

nav li a {
	background: #0952a4;
	border-right: 1px solid #fff;
	color: #fff;
	display: block;
	float: left;
	font: 400 13px/1.4 'Cutive', Helvetica, Verdana, Arial, sans-serif;
	padding: 10px;
	text-align: center;
	text-decoration: none;
	text-transform: uppercase;
	width: 12.5%;
	
	/*TRANSISTIONS*/
	-webkit-transition: background 0.5s ease;
	   -moz-transition: background 0.5s ease;
	     -o-transition: background 0.5s ease;
	    -ms-transition: background 0.5s ease;
	        transition: background 0.5s ease;
}

/*HOVER*/
nav li a:hover {
	background: orange;
}

/*SMALL*/
nav small {
	font: 100 11px/1 Helvetica, Verdana, Arial, sans-serif;
	text-transform: none;
	color: #aaa;
}

/*BORDER FIX*/
nav li:last-child a {
	border: none;
}

/*BLUE MENU*/
nav .blue {
	margin-top: 50px;
}

.blue li a {
	background: #75b1de;
}

.blue small {
	color: white;
}

.blue li a:hover {
	background: #444;
}

/*RED MENU*/

nav .red {
	margin-top: 50px;
}

.red li a {
	background: #5C0002;
}

.red small {
	color: white;
}

.red li a:hover {
	background: #a60306;
}

/* MEDIA QUERIES*/
@media only screen and (max-width : 1220px),
only screen and (max-device-width : 1220px){
	nav li a {
		font: 400 10px/1.4 'Cutive', Helvetica, Verdana, Arial, sans-serif;
	}
	
	nav small {
		font: 100 10px/1 Helvetica, Verdana, Arial, sans-serif;
	}
}

@media only screen and (max-width : 930px),
only screen and (max-device-width : 930px){
	nav li a {
		width: 25%;
		border-bottom: 1px solid #fff;
		font: 400 11px/1.4 'Cutive', Helvetica, Verdana, Arial, sans-serif;
	}
	
	nav li:last-child a, nav li:nth-child(4) a {
		border-right: none;
	}
	
	nav li:nth-child(5) a, nav li:nth-child(6) a, nav li:nth-child(7) a, nav li:nth-child(8) a {
		border-bottom: none;
	}
}

@media only screen and (max-width : 580px),
only screen and (max-device-width : 580px){
	nav li a {
		width: 50%;
		font: 400 12px/1.4 'Cutive', Helvetica, Verdana, Arial, sans-serif;
		padding-top: 12px;
		padding-bottom: 12px;
	}
	
	nav li:nth-child(even) a {
		border-right: none;
	}
	
	nav li:nth-child(5) a, nav li:nth-child(6) a {
		border-bottom: 1px solid #fff;
	}
}

@media only screen and (max-width : 320px),
only screen and (max-device-width : 320px){
	nav li a {
		font: 400 11px/1.4 'Cutive', Helvetica, Verdana, Arial, sans-serif;
	}
}
    </style>

<%--    <style type="text/css">
        @media only screen and (max-width: 600px) {
            .nav, .nav-tabs, .tabs-left {
                display: none;
            }
        }
    </style>--%>



    <style type="text/css">
        .tabs-left, .tabs-right {
            border-bottom: none;
            padding-top: 30px;
        }

        .tabs-left {
            border-right: 1px solid #ddd;
            margin-left: -28px;
        }

        @media only screen and (min-device-width: 320px) and (max-device-width: 500px) {
            .tabs-left {
                border-right: 1px solid #ddd;
                margin-left: 0px !important;
                display: flex !important;
            }

            .mob-tab {
                width: 54px !important;
                border-radius: 8px !important;
            }

            .labl {
                display:none !important;
            }

            .tabs-left > li {
                margin-right: 6px !important;
            }
        }

        .tabs-right {
            border-left: 1px solid #ddd;
        }

            .tabs-left > li, .tabs-right > li {
                float: none;
                margin-bottom: 2px;
            }

        .tabs-left > li {
            margin-right: -1px;
        }

        .tabs-right > li {
            margin-left: -1px;
        }

        .tabs-left > li.active > a,
        .tabs-left > li.active > a:hover,
        .tabs-left > li.active > a:focus {
            border-bottom-color: #ddd;
            border-right-color: transparent;
            color: #fff;
            cursor: pointer;
        }

        .tabs-right > li.active > a,
        .tabs-right > li.active > a:hover,
        .tabs-right > li.active > a:focus {
            border-bottom: 1px solid #ddd;
            border-left-color: transparent;
        }

        .tabs-left > li > a {
            border-radius: 10px 0 0 10px;
            margin-right: 0;
            display: block;
            color: #585858;
        }

        .tabs-right > li > a {
            border-radius: 0 4px 4px 0;
            margin-right: 0;
        }

        .sideways {
            margin-top: 50px;
            border: none;
            position: relative;
        }

            .sideways > li {
                height: 20px;
                width: 120px;
                margin-bottom: 100px;
            }

                .sideways > li > a {
                    border-bottom: 1px solid #ddd;
                    border-right-color: transparent;
                    text-align: center;
                    border-radius: 4px 4px 0px 0px;
                }

                .sideways > li.active > a,
                .sideways > li.active > a:hover,
                .sideways > li.active > a:focus {
                    border-bottom-color: transparent;
                    border-right-color: #ddd;
                    border-left-color: #ddd;
                }

            .sideways.tabs-left {
                left: -50px;
            }

            .sideways.tabs-right {
                right: -50px;
            }

                .sideways.tabs-right > li {
                    -webkit-transform: rotate(90deg);
                    -moz-transform: rotate(90deg);
                    -ms-transform: rotate(90deg);
                    -o-transform: rotate(90deg);
                    transform: rotate(90deg);
                }

            .sideways.tabs-left > li {
                -webkit-transform: rotate(-90deg);
                -moz-transform: rotate(-90deg);
                -ms-transform: rotate(-90deg);
                -o-transform: rotate(-90deg);
                transform: rotate(-90deg);
            }
    </style>



    <style>
        .content {
            background: #fff;
            border-right: 1px solid #ddd;
            border-left: 1px solid #ddd;
            border-bottom: 1px solid #ddd;
            position: relative;
            box-shadow: -2px 0 2px #eee, 2px 0 2px #eee;
            left: -15px;
        }

        @media only screen and (max-width: 600px) {
            .content {
                left: 0px;
            }

            .top {
                margin-top: 39px;
            }
        }
    </style>


    

    <style type="text/css">
        .k_nal {
            border-bottom-color: #ddd;
            width: 260px;
            z-index: 1011;
            background: #fff;
            color: #fff;
            /*border-bottom-color: #ddd;*/
            border: 1px solid #d4d4d4 !important;
}


  
    </style>

    <style type="text/css">
        marquee { background: #fff; }

.rightTI { background: #ff002b;
  white-space: nowrap; 
  overflow: hidden;
  animation: marquee 18s linear infinite;
}
.rightTI:hover {
  animation-play-state: paused;
}
@-webkit-keyframes marquee {
  0% {text-indent: 100%;}
  100% {text-indent: -100%;}
}

.rightCSS { 
  background: #a35dc1;
  overflow: hidden;
} 
.rightCSS div {
  position: relative;
  animation: CSSright linear 18s infinite;
} 
@keyframes CSSright {
  0% { right: -100% }
  100% { right: 100% }
}
.rightCSS:hover div {
  animation-play-state: paused;
}

.rightJS { background: #ffa900; }

.rightJQ { background: #00a753; }

.li {
  float: left;
  width: 100%;
 
  height: 20px;
 
  color: #0952a4;
}
    </style>


    <style>
        .kunal {
  display: inline-block;
  background-color: #0952a4;
  color: #fff;
  /*font-size: 1.5em;*/
  /*font-family: Courier, sans-serif;*/
      padding: 1px 7px 0px 6px;
  text-decoration: none;
  /*text-transform: uppercase;*/
  
  border-bottom: 0.25em solid #63acff;
  -moz-transition: 0.1s;
  -webkit-transition: 0.1s;
  transition: 0.1s;
  box-shadow: 0 2px 3px #ccc;
  position: relative;
}

.kunal:hover {
  background-color: #63acff;
  border-bottom-width: 0;
  margin-top: 0.2em;
  color:#fff;
}
    </style>


<style type="text/css">
        .photo {
  position: absolute;
  animation: round 16s infinite;
  
}

@keyframes round {
  25% {
    opacity: 1;
  }
  40% {
    opacity: 0;
  }
}
div:nth-child(1) {
  animation-delay: 1s;
}

div:nth-child(2) {
  animation-delay: 8s;
}

div:nth-child(3) {
  animation-delay: 1s;
}


    </style>





    <script type="text/javascript">
        /* Vanilla JS */

        var rightJS = {
            init: function () {
                rightJS.Tags = document.querySelectorAll('.rightJS');
                for (var i = 0; i < rightJS.Tags.length; i++) {
                    rightJS.Tags[i].style.overflow = 'hidden';
                }
                rightJS.Tags = document.querySelectorAll('.rightJS div');
                for (var i = 0; i < rightJS.Tags.length; i++) {
                    rightJS.Tags[i].style.position = 'relative';
                    rightJS.Tags[i].style.right = '-' + rightJS.Tags[i].parentElement.offsetWidth + 'px';
                }
                rightJS.loop();
            },
            loop: function () {
                for (var i = 0; i < rightJS.Tags.length; i++) {
                    var x = parseFloat(rightJS.Tags[i].style.right);
                    x++;
                    var W = rightJS.Tags[i].parentElement.offsetWidth;
                    var w = rightJS.Tags[i].offsetWidth;
                    if ((x / 100) * W > w) x = -W;
                    if (rightJS.Tags[i].parentElement.parentElement.querySelector(':hover') !== rightJS.Tags[i].parentElement) rightJS.Tags[i].style.right = x + 'px';
                }
                requestAnimationFrame(this.loop.bind(this));
            }
        };
        window.addEventListener('load', rightJS.init);

        /* JQUERY */

        $(function () {
            var rightJQ = {
                init: function () {
                    $('.rightJQ').css({
                        overflow: 'hidden'
                    });
                    $('.rightJQ').on('mouseover', function () {
                        $('div', this).stop();
                    });
                    $('.rightJQ').on('mouseout', function () {
                        $('div', this).animate({
                            right: '100%'
                        }, 15000, 'linear');
                    });
                    rightJQ.loop();
                },
                loop: function () {
                    $('.rightJQ div').css({
                        position: 'relative',
                        right: '-100%'
                    }).animate({
                        right: '100%'
                    }, 15000, 'linear', rightJQ.loop);
                }
            };
            rightJQ.init();
        });

    </script>


        <style type="text/css">
        .modal-body h1 {
  font-weight: 900;
  font-size: 2.3em;
  text-transform: uppercase;
}
.modal-body a.pre-order-btn {
  color: #000;
  background-color: gold;
  border-radius: 1em;
  padding: 1em;
  display: block;
  margin: 2em auto;
  width: 50%;
  font-size: 1.25em;
  font-weight: 6600;
}
.modal-body a.pre-order-btn:hover {
  background-color: #000;
  text-decoration: none;
  color: gold;
}

    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#myModal').modal('show');
        });
    </script>

    <div id="myModal" class="modal fade" role="dialog" style="display:none;">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
     
      </div>
      <div class="modal-body text-center">
       <img src="Images/gallery/Cloudy.jpeg" style="width:100%;"/>
      </div>
      <div class="modal-footer">
    
      </div>
    </div>

  </div>
</div>
    

    <!--New Design-->
    <div class="container top">
        <div class="row">
            <div class="col-sm-2">
                <ul class="nav nav-tabs tabs-left" role="tablist" style="margin-left: -28px;">
                    <li role="presentation" class="active"><a class="k_nal" href="#flight" aria-controls="home" role="tab" data-toggle="tab" style="width: 194px;"><span class="fa fa-plane"></span>&nbsp;  Flight</a></li>
                     <li role="presentation"><a class="k_nal" href="#fdep" aria-controls="home" role="tab" data-toggle="tab" style="width: 194px;"><span class="fa fa-plane"></span>&nbsp; Fixed Departure</a></li>
                    <li role="presentation"><a class="k_nal" href="#hotel" aria-controls="profile" role="tab" data-toggle="tab" style="width: 194px;"><span class="fa fa-bed" style="color: coral;"></span>&nbsp;  Hotel</a></li>
                    <li role="presentation"><a class="k_nal" href="#train" aria-controls="messages" role="tab" data-toggle="tab" style="width: 194px;"><span class="fa fa-bus" style="color: cadetblue;"></span>&nbsp;  Bus</a></li>
                    <%--                    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab" style="border-bottom-color: #ddd; width: 260px; z-index: 1;"><span class="fa fa-dashboard"></span>&nbsp;  Dashboard</a></li>--%>

                    <li role="presentation" class="cour-menu">
                        <a href="Dash.aspx" class="mm-arr k_nal" style="width:194px;z-index: 1;"><span class="fa fa-dashboard" style="color: darkmagenta;"></span>&nbsp;  Dashboard</a>
                      
                    </li>

                </ul>
            </div>
            <div class="col-sm-7 content">
                <div class="tab-content">
<div role="tabpanel" class="tab-pane active" id="flight">
                
                    <div><marquee class="li" direction=”right” onmouseover="stop()" onmouseout="start()">★ Book Flight Ticket and enjoy your holidays with distinctive experience ★</marquee></div>
                       
    <Search:IBESearch ID="IBESearch1" runat="server" />
                
    </div>
                 

                       <div role="tabpanel" class="tab-pane" id="fdep" style="height:395px;">
       
           <SearchDep:IBESearchDep runat="server" ID="FixDep" />  
                           </div>
                    <div role="tabpanel" class="tab-pane" id="hotel" style="height:395px;">
       
           <Search:HotelSearch runat="server" ID="HotelSearch2" />            

    </div>
      

  
                    <div role="tabpanel" class="tab-pane" id="train" style="height:395px;">
                       
        <Searchsss:BusSearch ID="Bus2" runat="server" />
                          

    </div>
       

   
                    <div role="tabpanel" class="tab-pane" id="settings">Dashboard</div>
     



                <div class="secndblak" style="display:none">
                    <div class="container">
                        <div class="col-xs-12 col-sm-12 tab-out bgclo">
                            <!-- Tab panes -->
                            <div class="tab-content tabsbg">
                                <div class="tab-pane active" id="Div1">
                                    <Search:IBESearch ID="IBESearch2" runat="server" />
                                </div>
                                <div class="tab-pane" id="Div2">
                                    <Search:HotelSearch runat="server" ID="HotelSearch1" />
                                </div>
                                <div class="tab-pane" id="Div3">
                                    <Searchsss:BusSearch runat="server" ID="BusSearch1" />
                                </div>

                                <div class="tab-pane" id="Div4">
                                    <SearchDep:IBESearchDep ID="IBESearchDep1" runat="server" />
                                </div>


                            </div>
                            <!-- Tab panes -->
                        </div>
                    </div>
                </div>
                
 </div>
            </div>

      

            
            
            <div class="col-md-3 tour_r" style="margin-top: 40px;">

                <!--====== TRIP INFORMATION ==========-->
                <%--<div class="tour_right tour_incl tour-ri-com">--%>
   <%--                 <h3><i class="fa fa-rss" style="color: coral;"></i>  News Feeds</h3>
                   <marquee height="100" width="100%" behavior="scroll" direction="up" scrollamount="2" onmouseover="this.stop();" onmouseout="this.start();">
  <ul>
    <li><a href="#">Hello I am Testing 1</a></li>
    <li><a href="#">Hello I am Testing 2</a></li>
    <li><a href="#">Hello I am Testing 3</a></li>
    <li><a href="#">Hello I am Testing 4</a></li>
</marquee>--%>
                    <%--<div class="col-md-3">--%>

                                            <div class="photo">
						<div class="to-ho-hotel-con">
							<div class="to-ho-hotel-con-1">
								<div class="hot-page2-hli-3"> <img src="Custom_Design/images/hci1.png" alt=""> </div>
								<div class="hom-hot-av-tic"> Book Now </div> <img src="Images/gallery/WhatsApp Image 2020-07-23 at 1.18.00 PM.jpeg" alt=""/> </div>
							<div class="to-ho-hotel-con-23">
								<div class="to-ho-hotel-con-2"> <a href="hotel-details.html"><h4>Special Diwali Fare</h4></a> </div>
								<div class="to-ho-hotel-con-3">
									<ul>
										<li>Call us on 8511177388/66
											
										</li>
										
									</ul>
								</div>
							</div>
						</div>
                                                    </div>

                                <div class="photo">
                               <div class="to-ho-hotel-con">
							<div class="to-ho-hotel-con-1">
								<div class="hot-page2-hli-3"> <img src="Custom_Design/images/hci1.png" alt=""> </div>
								<div class="hom-hot-av-tic"> Book Now </div> <img src="Images/gallery/WhatsApp Image 2020-10-19 at 4.47.43 PM.jpeg" alt=""/> </div>
							<div class="to-ho-hotel-con-23">
								<div class="to-ho-hotel-con-2"> <a href="hotel-details.html"><h4>Special Air Fare</h4></a> </div>
								<div class="to-ho-hotel-con-3">
									<ul>
										<li>Limited Seats availabel, call now.
											
										</li>
										
									</ul>
								</div>
							</div>
						</div>

                                                </div>

                      
					  
					   <div class="photo">
						<div class="to-ho-hotel-con">
							<div class="to-ho-hotel-con-1">
								<div class="hot-page2-hli-3"> <img src="Custom_Design/images/hci1.png" alt=""> </div>
								<div class="hom-hot-av-tic"> Book Now </div> <img src="Images/gallery/starair.jpg" alt=""/> </div>
							<div class="to-ho-hotel-con-23">
								
							</div>
						</div>
                                                    </div>
					<%--</div>--%>


                </div>


            </div>
        </div>
    <!--New Design-->
    <br />
    


   <nav>
	<ul>

 <%--       <li><button type="button" class="btn btn-secondary" data-container="body" data-toggle="popover" data-placement="top" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
  Popover on top
</button></li>--%>

     <%--   <li>
            <a tabindex="0"
   href="#" 
   role="button" 
   data-html="true" 
   data-toggle="popover" 
   data-placement="bottom" 
   data-trigger="focus" 
   title="<b>Example popover</b> - title" 
   data-content="<div><a href='/link'><b>Geoff</b> - content</a></div><div><b>Hilary</b> - content</div>">RSS <i class="fa fa-rss" aria-hidden="true"></i></a>
        </li>--%>

		<li><a href="#" data-container="body" data-toggle="popover" data-placement="top" title="Last Booking" data-content="DEL → BOM | 17 Mar, 2020 | SG-456"><i class="fa fa-plane" aria-hidden="true"></i>  Last Searching</a></li>
        <li><a href="Report/TicketReport.aspx" > <i class="fa fa-book" ></i>  Last Booking</a></li>
		<li><a href="Report/Agent/TravelCalender.aspx" style="width: 160px;"><i class="fa fa-calendar" aria-hidden="true"></i>  Travel Calendar</a></li>
		<li><a href="Report/Accounts/BankDetails.aspx"><i class="fa fa-bank" aria-hidden="true"></i>  Bank Details</a></li>
		<li><a href="Report/Accounts/LedgerSingleOrderID.aspx"><i class="fa fa-file" aria-hidden="true"></i>  Ledger Details</a></li>
		<li><a href="Report/Accounts/AgentPanel.aspx"><i class="fa fa-inr" aria-hidden="true"></i>  Upload Amount</a></li>
		<asp:HiddenField ID="hdlastsearch" runat="server" />
	</ul>
</nav>

    <br />


<section class="mobiApp">
 	<div class="container">
 		<div class="row">
 			<div class="u_floatL col-md-4" style="position: relative;top: -68px;">
 				<img src="Images/Abstract/app2.png" style="width: 235px;"/>
 			</div>
 			<div class="u_floatL">
 				<h4 class="head">Over <span class="u_clViaWhite u_fontW700">1 million</span> customers use our mobile app</h4>
 				<div class="row">
 					<div class="u_inlineblk">
 						<div class="appDisc">
 							<h5>Download RichaTravel app now &amp;</h5>
 							<p>
 								    Experience <i class="INR u_fontW100"></i><span class="u_clViaWhite u_fontW600"> Richa Travel. </span> in your android device<br>
<%-- 								Flat<span class="u_clViaWhite u_fontW600"> 6% OFF* </span> on Bus Tickets--%>
 							</p>
 						</div>
 					</div>
 					<div class="u_inlineblk">
						<div class="appVia">
	 						<h5 class="u_alignC">Download APK File</h5>
	 						
	 							<a href="../Custom_Design/RichaTravels.apk" class="u_clViaWhite">Get File!</a>
	 						
 						</div>
 					</div>
 				</div>
 				<div class="row appStore">
 					<div class="u_inlineblk">
 						<a href="//play.google.com/store/apps/details?id=app.via&amp;referrer=utm_source%3Dwebsitefooter" target="_blank">
							<div class="spriteIN GooglePlay-small"></div>
						</a>
 					</div>
 					<div class="u_inlineblk">
 						<a href="//appsto.re/in/r6qG9.i" target="_blank">
						    <div class="spriteIN Istore-small"></div>
						</a>
 					</div>
 				</div>
 			</div>
 			<div class="u_clear"></div>
 		</div>
 	</div>
 </section>






    <div id="carousel-example-generic searchbg " class="carousel slide" data-ride="carousel" style="display: none;">

        <div class="clearfix"></div>

        <div class="carousel slide" id="carousel-example-captions" data-ride="carousel">

            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img id="Img1" runat="server" src="~/images/img-1.jpg" alt="1" style="width: 100%;" />
                    <div class="carousel-caption">
                        <div class="toptxt">ACCOMMODATION NOW OPEN!</div>
                        <span class="toptxt">From INR 2999</span>
                    </div>
                </div>
                <div class="item">
                    <img id="Img2" runat="server" src="~/images/img-2.jpg" alt="1" style="width: 100%;" />
                    <div class="carousel-caption">
                        <div class="toptxt">BOOK NOW THE BEST DEALS</div>
                        <span class="toptxt">Book securely and with confidence</span>
                    </div>
                </div>
                <div class="item">
                    <img id="Img3" runat="server" src="~/images/img-3.jpg" alt="1" style="width: 100%;" />
                    <div class="carousel-caption">
                        <div class="toptxt">ACCOMMODATION NOW OPEN!</div>
                        <span class="toptxt">From INR 2999</span>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="searchengine searchbg " id="multisit">
                <div class="container srcmobile ">
                    <div class="row" style="margin-bottom: 0px;">
                        <div style="clear: both;"></div>
                        <div class="col-xs-12 col-sm-12" style="padding-right: 0px;">
                            <ul class="nav nav-tabs tabstab">
                                <li class="active "><a href="#home" data-toggle="tab">
                                    <span class="sprte iconcmn icnhnflight"></span>
                                    Flight
                                </a></li>
                                <li class=" "><a href="#profile" data-toggle="tab">
                                    <span class="sprte iconcmn icnhtl" aria-hidden="true"></span>
                                    Hotel
                                </a></li>
                                <li class=" "><a href="#bus" data-toggle="tab">
                                    <span class="sprte iconcmn icnhnbus" aria-hidden="true"></span>
                                    Bus
                                </a></li>

                                <li class=" "><a href="https://www.irctc.co.in/nget/train-search" target="_blank">
                                    <span class="sprte iconcmn icnhnhlydy" aria-hidden="true"></span>
                                    Train
                                </a></li>
                                <li class=" "><a href="#dep" data-toggle="tab">
                                    <span class="sprte iconcmn icnhnflight" aria-hidden="true"></span>
                                    Fixed Departure
                                </a></li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="secndblak">
                    <div class="container">
                        <div class="col-xs-12 col-sm-12 tab-out bgclo">
                            <!-- Tab panes -->
                            <div class="tab-content tabsbg">
                                <div class="tab-pane active" id="home">
                                    <Search:IBESearch ID="IBE_CP" runat="server" />
                                </div>
                                <div class="tab-pane" id="profile">
                                    <Search:HotelSearch runat="server" ID="HotelSearch" />
                                </div>
                                <div class="tab-pane" id="bus">
                                    <Searchsss:BusSearch runat="server" ID="BusSearch" />
                                </div>

                                <div class="tab-pane" id="dep">
                                    <SearchDep:IBESearchDep ID="FixDep1" runat="server" />
                                </div>


                            </div>
                            <!-- Tab panes -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <div class="booking-remarks" id="divStockistList" style="display: none;">
        <img id="imgCloseCHSch" src="Images/closebox.png" alt="" style="float: right; z-index: 9999; cursor: pointer; margin-top: -27px; margin-right: -20px;"
            title="Close" />

    </div>
    <div class="row" style="display: none;">
        <div id="toPopup" class="tbltbl large-12 medium-12 small-12">
            <div class="close">
            </div>
            <span class="ecs_tooltip">Press Esc to close <span class="arrow"></span></span>
            <div id="popup_content">
                <!--your content start-->
                <table border="0" cellpadding="10" cellspacing="5" style="font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: normal; font-style: normal; color: #000000">
                    <tr>
                        <td>
                            <b>PNR :</b> <span id="PNR"></span>
                            <input id="txtPNRNO" name="txtPNRNO" type="hidden" />
                        </td>
                        <td id="TktNoInfo" style="display: none;">
                            <b>Ticket No:</b> <span id="TktNo"></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="display: none;" id="PaxnameInfoResu">
                            <b>PAX NAME :</b> <span id="Paxname"></span>
                        </td>
                        <td style="display: none;" id="PaxnameInfoRefnd">
                            <div id="Refunddtldata" class="large-12 medium-12 small-12"></div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <b>
                                <span id="RemarksTypetext"></span>Remark 
                            </b>
                            <input id="RemarksType" name="RemarksType" type="hidden" />

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <textarea id="txtRemark" name="txtRemark" cols="56" rows="1" style="border: thin solid #808080"></textarea>
                        </td>
                    </tr>
                    <tr id="trCancelledBy" visible="false">
                        <td>
                            <b>Cancelled By:</b>
                        </td>
                        <td>
                            <asp:DropDownList ID="DrpCancelledBy" CssClass="drop" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div style="width: 20%; margin-left: 40%;">
                                <asp:Button ID="btnRemark" runat="server" Text="Submit" CssClass="buttonfltbk rgt w20" />
                                <input id="txtPaxid" name="txtPaxid" type="hidden" />
                                <input id="txtPaxType" name="txtPaxType" type="hidden" />
                                <input id="txtSectorid" name="txtSectorid" type="hidden" />
                                <input id="txtOrderid" name="txtOrderid" type="hidden" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <!--your content end-->
        </div>

        <div class="loader">
        </div>
        <div id="backgroundPopup">
        </div>
        <div id="HourDeparturePopup">
            <div class="close11">
            </div>
            <span class="ecs_tooltip">Press Esc to close <span class="arrocol-md-4 col-sm-6 col-xs-5 nopadw"></span></span>
            <div class="HourDeparturepopup_content">

                <div class="large-12 medium-12 small-12">
                    <div class="clear"></div>
                    <div class="large-12 medium-12 small-12 text-center ">Click “OK” to proceed for offline request.</div>
                    <div class="clear"></div>
                    <div class="large-4 medium-4 small-4"></div>
                    <div style="margin-left: 100px;" class="rgt w100 buttonfltbkss">OK</div>
                    <input id="txtPaxid_4HourDeparture" name="txtPaxid_4HourDeparture" type="hidden" />
                </div>
            </div>
        </div>


        <div id="htlRfndPopup">
            <div class="refundbox">

                <div style="font-weight: bold; font-size: 16px; text-align: center; width: 100%;">

                    <div id="RemarkTitle"></div>
                    <div style="float: right; width: 20px; height: 20px; margin: -20px -13px 0 0;">
                        <a href="javascript:ShowHide('hide');">
                            <img src="<%=ResolveUrl("~/Images/close.png") %>" height="20px" /></a>
                    </div>

                </div>

                <div class="large-12 medium-12 small-12">

                    <div class="laege-1 medium-1 small-1 columns bld">Hotel Name:</div>
                    <div class="laege-5 medium-5 small-5 columns" id="HotelName"></div>

                    <div class="clear"></div>
                    <div class="clear"></div>
                    <div class="laege-1 medium-1 small-1 columns bld">
                        Net Cost:
                    </div>
                    <div class="laege-1 medium-1 small-1 columns" id="amt"></div>
                    <div class="large-1 medium-1 small-1  medium-push-1 columns bld">
                        No. of Room:
                    </div>
                    <div class="laege-1 medium-1 small-1 large-push-1 medium-push-1 columns" id="room"></div>

                    <div class="laege-1 medium-1 small-1 large-push-1 medium-push-1 columns bld">No. of Night:</div>
                    <div class="laege-1 medium-1 small-1 large-push-1 medium-push-1 columns" id="night"></div>
                    <div class="laege-1 medium-1 small-1 columns bld">No. of Adult:</div>
                    <div class="laege-1 medium-1 small-1 columns" id="adt"></div>
                    <div class="laege-1 medium-1 small-1 large-push-1 medium-push-1 columns bld">No. of Child:</div>
                    <div class="laege-1 medium-1 small-1 large-push-1 medium-push-1 columns end" id="chd"></div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <div id="policy" class="large-12 medium-12 small-12"></div>

                <div class="clear"></div>
                <div class="large-12 medium-12 small-12">

                    <div style="font-weight: bold;">
                        Cancellation Remark:
                    </div>
                    <div>
                        <textarea id="txtRemarkss" cols="40" rows="2" name="txtRemarkss"></textarea>
                    </div>
                    <div style="float: right; padding-left: 40px;">
                        <asp:Button ID="btn_Refund" runat="server" Text="Hotel Cancelltion" CssClass="button" ToolTip="Auto Cancel of Hotel"
                            OnClientClick="return RemarkValidation('cancellation')" />
                    </div>
                    <div class="clear1"></div>
                </div>

                <div>
                    <input id="StartDate" type="hidden" name="StartDate" />
                    <input id="EndDate" type="hidden" name="EndDate" />
                    <input id="Parcial" type="hidden" name="Parcial" value="false" />
                    <input id="OrderIDS" type="hidden" name="OrderIDS" />
                </div>
                <div style="visibility: hidden;">

                    <div style="font-weight: bold;">
                        Full Cancellation
                                                        <input id="ChkFullCan" type="radio" name="Can" checked="checked" />
                    </div>
                    <div style="font-weight: bold; padding-left: 20px;">
                        Partial Cancellation
                                                        <input id="ChkParcialCan" type="radio" name="Can" />
                    </div>

                    <%-- <tr><td colspan="3" class="PrcialRegardCancelMsg"></td></tr>--%>
                </div>
            </div>
        </div>
    </div>


 

    <%-- <link href="<%=ResolveUrl("~/CSS/PopupStyle.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/Styles/jAlertCss.css")%>" rel="stylesheet" />

    <script src="<%=ResolveUrl("~/Hotel/JS/HotelRefund.js")%>" type="text/javascript"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/PopupScript.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/alert.js")%>"></script>--%>
    <%--    <script type="text/javascript">
        $("#rdbMultiCity").click(function () {

            $("#multisit").removeClass("searchengine searchbg").addClass("searchenginess searchbg ");
            $(".toptxt").hide();

        });

        $(".toptxt").show();

        //$('.carousel').carousel({
        //    interval: 1000
        //})

        if ('<%=Request("Htl")%>' == 'H') {
            $("#img2-").show();
            $("#img1").hide();
        }
        else {
            $("#img1").show();
            $("#img2").hide();
        }


    </script>--%>
    <%--<script type="text/javascript">

        $(document).ready(function () {


            $(".onewayss").removeClass("col-md-4 nopad text-search mltcs").addClass("col-md-5 nopad text-search mltcs");

            $("#rdbOneWay").click(function () {

                $(".onewayss").removeClass("col-md-4 nopad text-search mltcs").addClass("col-md-5 nopad text-search mltcs");
            });

            $("#rdbRoundTrip").click(function () {

                $(".onewayss").removeClass("col-md-5 nopad text-search mltcs").addClass("col-md-4 nopad text-search mltcs");
            });
            $("#rdbMultiCity").click(function () {
                $(".onewayss").removeClass("col-md-4 nopad text-search mltcs").addClass("col-md-5 nopad text-search mltcs");
            });




            $("#CB_GroupSearch").click(function () {
                if ($(this).is(":checked")) {
                    // $("#box").hide();
                    $(".Traveller").hide();
                    $("#rdbRoundTrip").attr("checked", true);
                    $("#rdbOneWay").attr("checked", false);

                } else {
                    // $("#box").show();
                    $(".Traveller").show();
                }
            });
        });
    </script>--%>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.dropdown-submenu a.test').on("click", function (e) {
                $(this).next('ul').toggle();
                e.stopPropagation();
                e.preventDefault();
            });
        });
    </script>

<%--    <script type="text/javascript">

        $('.btn').on('click', function () {
            var $this = $(this);
            $this.button('loading');
            setTimeout(function () {
                $this.button('reset');
            }, 8000);
        });
</script>--%>

<script type="text/javascript">
    $(function () {
        debugger;
        // Enables popover
        $("[data-toggle=popover]").popover();
        container: 'body'
        $("lsearch").prepend($("hdlastsearch").val);
    });
</script>

    

    <%-- <script type="text/javascript" src="<%=ResolveUrl("js/jquery-latest.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("js/bootstrap.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("js/wow.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("js/materialize.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("js/custom.js")%>"></script>--%>
</asp:Content>
