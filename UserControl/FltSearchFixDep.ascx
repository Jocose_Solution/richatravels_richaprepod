﻿<%@ Control Language="VB" AutoEventWireup="true" CodeFile="FltSearchFixDep.ascx.vb" Inherits="UserControl_FltSearchFixDep" %>

<%@ Register Src="~/UserControl/HotelSearch.ascx" TagPrefix="uc1" TagName="HotelSearch" %>
<%@ Register Src="~/UserControl/HotelDashboard.ascx" TagPrefix="uc1" TagName="HotelDashboard" %>

<link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
    rel="stylesheet" />

<div class="row" style="padding: 15px 0px 10px 0px;">
    <div class="topwaysF"  style="display:none">

        <div class="col-md-1 col-xs-4 nopad text-search">
            <label class="mail active">
                <input type="radio" style="display: none;" name="TripTypeF" value="rdbOneWayF" id="rdbOneWayF" checked="checked" />
                One Way</label>
        </div>
        <div class="col-md-2 col-xs-4 nopad text-search">
            <label class="mail">
                <input type="radio" name="TripTypeF" style="display: none;" value="rdbRoundTripF" id="rdbRoundTripF" />
                Round Trips</label>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var selector = '.topwaysF div label'; //mk
            $(selector).bind('click', function () {
                $(selector).removeClass('active');
                $(this).addClass('active');
            });

        });
    </script>
    <%--<div class="col-md-4 col-xs-6 text-search">

        <input id="CB_GroupSearch" type="checkbox" value="grpSearch" name="GroupSearch" />
        Group Search       
    </div>--%>
</div>
<div style="position: relative">
    <div class="col-md-9 nopad">
        <div class="col-md-12 nopad">
            <div class="col-md-8 nopad text-search mltcs">

                
                <div class="form-group">
                    <label for="exampleInputEmail1">
                        Select Sector:</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-map-marker fa-lg yellow"></i>
                        </div>
                        <asp:DropDownList ID="Sector" runat="server" class="form-control">
                        </asp:DropDownList>
                    </div>
                </div>

                <div class="form-group" style="display:none">
                    <label for="exampleInputEmail1">
                        Search Sector:</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-map-marker fa-lg yellow"></i>
                        </div>
                        <input type="text"  name="txtDepCityFD" class="form-control" placeholder="Enter Your Departure City" value="" onclick="this.value = '';" id="txtDepCityFD" />
                        <input type="hidden" id="hidtxtDepCityFD" name="hidtxtDepCityFD" value="TT" />
                    </div>
                </div>



                <div class="form-group" style="display:none">
                    <label for="exampleInputEmail1">
                        Leaving From:</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-map-marker fa-lg yellow"></i>
                        </div>
                        <input type="text" name="txtDepCity1F" class="form-control" placeholder="Enter Your Departure City" value="New Delhi, India-Indira Gandhi Intl(DEL)" onclick="this.value = '';" id="txtDepCity1F" />
                        <input type="hidden" id="hidtxtDepCity1F" name="hidtxtDepCity1F" value="ZZZ,IN" />
                    </div>
                </div>
            </div>
            <div class="onewayss col-md-5 nopad text-search mltcs" style="display:none">
                <div class="form-group">
                    <label for="exampleInputEmail1">
                        Going To:</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-map-marker fa-lg yellow"></i>
                        </div>
                        <input type="text" name="txtArrCity1F" onclick="this.value = '';" id="txtArrCity1F" value="New Delhi, India-Indira Gandhi Intl(DEL)" class="form-control" placeholder="Enter Your Destination City" />
                        <input type="hidden" id="hidtxtArrCity1F" name="hidtxtArrCity1F" value="ZZZ,IN" />
                    </div>
                </div>
            </div>
            <div class="col-md-2 nopad text-search mrgs" id="oneF" style="display:none">
                <div class="form-group">
                    <label for="exampleInputEmail1">
                        Depart Date:</label>
                    <div class="input-group">

                        <div class="input-group-addon">
                            <i class="fa fa-calendar yellow"></i>
                        </div>
                        <input type="text" class="form-control" placeholder="dd/mm/yyyy" name="txtDepDateF" id="txtDepDateF" value=""
                            readonly="readonly" />
                        <input type="hidden" name="hidtxtDepDate" id="hidtxtDepDateF" value="" />

                    </div>
                </div>
            </div>
            <div class="col-md-2 nopad text-search mrgs" id="ReturnF" style="display:none">
                <div class="form-group" id="trRetDateRowF" style="display: none;">
                    <label for="exampleInputEmail1">
                        Return Date:</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar yellow"></i>
                        </div>
                        <input type="text" placeholder="dd/mm/yyyy" name="txtRetDateF" id="txtRetDateF" class=" form-control" value=""
                            readonly="readonly" />
                        <input type="hidden" name="hidtxtRetDateF" id="hidtxtRetDateF" value="" />


                    </div>
                </div>
            </div>
            <div style="display: none;" id="twoF">
                <div class="onewayss col-md-4 nopad text-search mltcs" id="DivDepCity2F">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtDepCity2F" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity2F" />
                            <input type="hidden" id="hidtxtDepCity2F" name="hidtxtDepCity2F" value="" />
                        </div>
                    </div>
                </div>

                <div class="onewayss col-md-4 nopad text-search mltcs">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtArrCity2F" class="form-control" placeholder="Enter Your Destination City" id="txtArrCity2F" />
                            <input type="hidden" id="hidtxtArrCity2F" name="hidtxtArrCity2F" value="" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2 nopad text-search mrgs" id="DivArrCity2F">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar yellow"></i>
                            </div>
                            <input type="text" name="txtDepDate2" id="txtDepDate2F" class=" form-control" placeholder="dd/mm/yyyy" readonly="readonly" value="" />
                            <input type="hidden" name="hidtxtDepDate2F" id="hidtxtDepDate2F" value="" />
                        </div>
                    </div>
                </div>
            </div>

            <div style="display: none;" id="threeF">

                <div class="onewayss col-md-4 nopad text-search mltcs" id="DivDepCity3F">
                    <div class="form-group">

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtDepCity3F" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity3F" />
                            <input type="hidden" id="hidtxtDepCity3F" name="hidtxtDepCity3F" value="" />
                        </div>
                    </div>
                </div>
                <div class="onewayss col-md-4 nopad text-search mltcs">
                    <div class="form-group">

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtArrCity3F" class="form-control" placeholder="Enter Your Destination City" id="txtArrCity3F" />
                            <input type="hidden" id="hidtxtArrCity3F" name="hidtxtArrCity3F" value="" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2 nopad text-search mrgs" id="DivArrCity3F">
                    <div class="form-group">

                        <div class="input-group">

                            <div class="input-group-addon">
                                <i class="fa fa-calendar yellow"></i>
                            </div>
                            <input type="text" name="txtDepDate3F" id="txtDepDate3F" class="form-control" placeholder="dd/mm/yyyy" readonly="readonly" />
                            <input type="hidden" name="hidtxtDepDate3F" id="hidtxtDepDate3F" value="" />
                        </div>
                    </div>
                </div>
            </div>

            <div style="display: none;" id="four">
                <div class="onewayss col-md-4 nopad text-search mltcs" id="DivDepCity4F">
                    <div class="form-group">

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtDepCity4F" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity4F" />
                            <input type="hidden" id="hidtxtDepCity4F" name="hidtxtDepCity4F" value="" />
                        </div>
                    </div>
                </div>
                <div class="onewayss col-md-4 nopad text-search mltcs">
                    <div class="form-group">

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtArrCity4F" class="form-control" placeholder="Enter Your Destination City" id="txtArrCity4F" />
                            <input type="hidden" id="hidtxtArrCity4F" name="hidtxtArrCity4F" value="" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2 nopad text-search mrgs" id="DivArrCity4F">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar yellow"></i>
                            </div>
                            <input type="text" name="txtDepDate4F" id="txtDepDate4F" class="form-control" placeholder="dd/mm/yyyy" readonly="readonly" />
                            <input type="hidden" name="hidtxtDepDate4F" id="hidtxtDepDate4F" value="" />
                        </div>
                    </div>
                </div>
            </div>

            <div style="display: none;" id="five">
                <div class="onewayss col-md-4 nopad text-search mltcs" id="DivDepCity5">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtDepCity5F" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity5F" />
                            <input type="hidden" id="hidtxtDepCity5F" name="hidtxtDepCity5F" value="" />
                        </div>
                    </div>
                </div>
                <div class="onewayss col-md-4 nopad text-search mltcs">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtArrCity5F" class="form-control" placeholder="Enter Your Destination City" onclick="this.value = '';" id="txtArrCity5F" />
                            <input type="hidden" id="hidtxtArrCity5F" name="hidtxtArrCity5F" value="" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2 nopad text-search mrgs" id="DivArrCity5F">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar yellow"></i>
                            </div>
                            <input type="text" name="txtDepDate5" id="txtDepDate5F" class="form-control" placeholder="dd/mm/yyyy" readonly="readonly" />
                            <input type="hidden" name="hidtxtDepDate5F" id="hidtxtDepDate5F" value="" />
                        </div>
                    </div>
                </div>
            </div>

            <div style="display: none;" id="six">
                <div class="onewayss col-md-4 nopad text-search mltcs" id="DivDepCity6F">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtDepCity6F" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity6F" />
                            <input type="hidden" id="hidtxtDepCity6F" name="hidtxtDepCity6" value="" />
                        </div>
                    </div>
                </div>

                <div class="onewayss col-md-4 nopad text-search mltcs">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtArrCity6F" class="form-control" placeholder="Enter Your Destination City" onclick="this.value = '';" id="txtArrCity6F" />
                            <input type="hidden" id="hidtxtArrCity6F" name="hidtxtArrCity6F" value="" />
                        </div>
                    </div>
                </div>
                <div class="col-md-2 nopad text-search mrgs" id="ArrCity6F">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar yellow"></i>
                            </div>
                            <input type="text" name="txtDepDate6F" id="txtDepDate6F" class="form-control" placeholder="dd/mm/yyyy" readonly="readonly" />
                            <input type="hidden" name="hidtxtDepDate6F" id="hidtxtDepDate6F" value="" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="clear"></div>

            <div class="row col-md-5 col-xs-12 pull-right" id="addF" style="display:none">
                <div class="col-md-4 col-xs-4 text-search text-right">
                    <a id="plusF" class="pulse text-search">Add City</a>
                </div>
                <div class="col-md-4 col-xs-4 text-search  text-right">
                    <a id="minusF" class="pulse text-search">Remove City</a>
                </div>
            </div>
        </div>
       
        <div class="row" style="display:none">
        <div class="text-search col-md-5 col-xs-12" style="padding-bottom: 10px; cursor: pointer;" id="advtravelF">Advanced options <i class="fa fa-chevron-right" aria-hidden="true"></i></div>
        <div class="col-md-12 advopt" id="advtravelssF" style="display: none;  overflow: auto;">
            <div class="col-md-3 nopad text-search">
                <div class="form-group">
                    <label for="exampleInputEmail1">
                        Airlines</label>
                    <input type="text" placeholder="Search By Airlines" class="form-control" name="txtAirline" value="" id="txtAirlineF" />
                    <input type="hidden" id="hidtxtAirlineF" name="hidtxtAirlineF" value="" />

                </div>
            </div>
            
            <div class="col-md-3 col-xs-12 text-search">
                <div class="form-group">
                    <label for="exampleInputEmail1">
                        Class Type</label>
                    <select name="CabinF" class="form-control" id="CabinF">
                        <option value="" selected="selected">--All--</option>
                        <option value="C">Business</option>
                        <option value="Y">Economy</option>
                        <option value="F">First</option>
                        <option value="W">Premium Economy</option>
                    </select>

                </div>
            </div>
        </div>
    </div>

    </div>

   
    <div class="col-md-3 nopad mrgss"     style="margin-left: -221px;">

        <div class="col-md-6 nopad text-search" style="cursor: pointer;" id="TravellerF">
            <div class="form-group">
                <label for="exampleInputEmail1">
                    Traveller</label>
                <div class="input-group">

                    <div class="input-group-addon">
                        <i class="fa fa-user yellow" aria-hidden="true"></i>
                    </div>
                    <input type="text" class="form-control" id="sapnTotPaxF" placeholder=" Traveller">
                </div>
            </div>
        </div>

     
        <div class="col-md-6 nopad ">
               <input type="checkbox" class="chkNonstop" name="chkNonstop" id="chkNonstop" value="Y"  style="display:none"/>
            <button type="button" id="btnSearchF" name="btnSearchF" value="Search" class="btn btn-danger" style="margin-top: 24px;">
                Search Flights</button>
        </div>
    </div>
    <div class="clear1"></div>
    
    <div class="clear1"></div>
    <div class="col-md-3 col-xs-12 text-search" id="trAdvSearchRowF" style="display: none">
        <div class="lft ptop10">
            All Fare Classes
        </div>
        <div class="lft mright10">
              
            <input type="checkbox" name="chkAdvSearchF" id="chkAdvSearchF" value="True" />
        </div>
        <div class="large-4 medium-4 small-12 columns">
            Gds Round Trip Fares
                                
                                <span class="lft mright10">
                                    <input type="checkbox" name="GDS_RTFF" id="GDS_RTFF" value="True" />
                                </span>
        </div>

        <div class="large-4 medium-4 small-12 columns">
            Lcc Round Trip Fares
                                
                                <span class="lft mright10">
                                    <input type="checkbox" name="LCC_RTFF" id="LCC_RTFF" value="True" />
                                </span>
        </div>

    </div>
</div>
<script>
    $(document).ready(function () {
        $("#advtravelF").click(function () {
            $("#advtravelssF").slideToggle();
        });


        $("#TravellerF").click(function () {
            $("#boxF").slideToggle();
        });
        $("#serachbtnF").click(function () {
            $("#boxF").slideToggle();
        });

    });
</script>
<div id="div_Adult_Child_Infant">
    <div class="row" id="boxF">
        <i class="" aria-hidden="true"></i>
        <div class="col-md-7 col-xs-12 text-searchs">
            <div class="form-group">
                <div class="col-md-5 col-xs-5 travelpad">
                    <label class="lblss" for="exampleInputEmail1">
                        Adult (12+) Yrs
                    </label>
                </div>
                <div class="col-md-7 col-xs-7 ">
                    <select class="form-control" name="AdultF" id="AdultF">
                        <option value="1" selected="selected">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                    </select>
                </div>

            </div>
        </div>
        <div class="col-md-7 col-xs-12 text-searchs">
            <div class="form-group">
                <div class="col-md-5 col-xs-5 travelpad">
                    <label class="lblss" for="exampleInputEmail1">
                        Child(2-12) Yrs</label>
                </div>
                <div class="col-md-7 col-xs-7 ">
                    <select class="form-control" name="ChildF" id="ChildF">
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-7 col-xs-12 text-searchs">
            <div class="form-group">
                <div class="col-md-5 col-xs-5 travelpad">
                    <label class="lblss" for="exampleInputEmail1">
                        Infant(0-2) Yrs</label>
                </div>
                <div class="col-md-7 col-xs-7 ">
                    <select class="form-control" name="InfantF" id="InfantF">
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-xs-12">
            <button type="button" onclick="plusF()" class="btn btn-danger" id="serachbtnF" style="margin-top: 5px;">
                Done</button>
        </div>

    </div>
</div>






<script type="text/javascript">
    function plusF() {
        document.getElementById("sapnTotPaxF").value = (parseInt(document.getElementById("AdultF").value.split(' ')[0]) + parseInt(document.getElementById("ChildF").value.split(' ')[0]) + parseInt(document.getElementById("InfantF").value.split(' ')[0])).toString() + ' Traveller';
    }
    plusF();
</script>
<script type="text/javascript">
    var myDate = new Date();
    var currDate = (myDate.getDate()) + '/' + (myDate.getMonth() + 1) + '/' + myDate.getFullYear();
    document.getElementById("txtDepDateF").value = currDate;
    document.getElementById("hidtxtDepDateF").value = currDate;
    document.getElementById("txtRetDateF").value = currDate;
    document.getElementById("hidtxtRetDateF").value = currDate;
    var UrlBase = '<%=ResolveUrl("~/") %>';
</script>

<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/search3F.js") %>"></script>

<script type="text/javascript">

    $(function () {
        $("#CB_GroupSearchF").click(function () {

            if ($(this).is(":checked")) {
                // $("#box").hide();
                $("#TravellerF").hide();
                $("#rdbRoundTripF").attr("checked", true);
                $("#rdbOneWayF").attr("checked", false);

            } else {
                // $("#box").show();
                $("#TravellerF").show();
            }
        });
    });
</script>
